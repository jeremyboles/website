WATTLE ?= wattle
PRECOMPRESS_FIND = -iname "*.css" -o -iname "*.html" -o -name "*.svg"

# src = css/ static/ journal/ templates/ wiki/
src = css/ static/ templates/

.PHONY: all
all: clean archive build compress 
	@:

archive: $(src)
	$(WATTLE) archive
	cp archive.tar.gz static/

build: $(src)
	$(WATTLE) build

clean:
	rm -f archive.tar.gz
	rm -f static/archive.tar.gz
	rm -rf public


compress: compress_br compress_gzip compress_zstd

compress_br: build
	find ./public/ -type f \( $(PRECOMPRESS_FIND) \) \
		-print0 -exec brotli --best --force "{}" \;

compress_gzip: build
	find ./public/ -type f \( $(PRECOMPRESS_FIND) \) \
		-print0 -exec gzip -9 -f -k "{}" \;

compress_zstd: build
	find ./public/ -type f \( $(PRECOMPRESS_FIND) \) \
		-print0 -exec zstd -19 -f -k "{}" \;

upload:
	rsync --delete \
		--human-readable --no-whole-file --recursive --stats --update \
		--chmod=Du=rwx,Dg=rwx,Do=rx,Fu=rw,Fg=rw,Fo=r --chown=jeremy:www \
		public/ web.straggly.tech:/srv/www
