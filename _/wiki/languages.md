# Programming Languages

---

![GRAPHIC: A woodcut illustration of a Tabanidae](image:clegs/resized)

I have been trying to simplify my stack lately so, these days, I try and a pick a programming language that will suit the particular problem I am working on out of the following languages: 

* ANSI **[[C]]** -- 1972 - United States
* **[[Lua]]** -- 1993 - Brazil
* **[[Erlang]]** -- 1986 - Sweden
* **[[Go]]** -- 2009 - United States
* POSIX **[[Shell]]** Script -- 1979 - United States
