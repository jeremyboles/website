# Word Processor
## Word processors are computer programs that can help one manage extended pieces of writing.

---

The definition of a word processor is somewhat blurry. In my mind, what makes a word processor different than a text editor is it's ability to manage the _structure_ of multiple sections of a larger work of prose. Note that this definition does not require a word processor to have any formatting capabilities.

## Hardware

- The **Canon Cat** was a short-lived _computer appliance_ that was designed by team led by Jef Raskin. It is notable for its addition of _LEAP keys_ that were placed below the space bar and allow people to navigate through a document quickly by typing where they wanted to navigate to.
- The **Wang WPS** was the first CRT based word processor and was popular in the United States in the early 1980s.
- **AlphaSmart** had a number of word processing machines that used a small LCD to display a few lines of the text the person was writing.

## Software

There are common, _kitchen-sink_ word processing programs that most are already familiar with and are not very interesting to my research, so they will not be listed here.

### CLI Programs

There programs are ones that run in a terminal or console. These are often light-weight and excel at maximizing keyboard usage.

- **WordStar** is a beloved word processor that runs on MS-DOS. It is notable for still being used by author George R.R. Martin. There is a clone of WordStar called [WordTsar](http://wordtsar.ca/).
- [**WordGrinder**](http://cowlark.com/wordgrinder/) is word processor for the terminal that is described as being designed to "...get the hell out of your way and let you get some work done".


### GUI Programs

The programs resemble what most think of when they think of word processors.

- [**iA Writer**](https://ia.net/writer) is a popular macOS application which uses Markdown as its document format.
- [**Scrivner**](https://www.literatureandlatte.com/scrivener) is a word processor that focuses on writing book-length works and collecting keeping research material along side of one's prose.
- [**Bitters**](https://m15o.ichi.city/bitters/) is a text editor that is heavily inspired by the Canon Cat. It has an implementation of the Cat's _LEAP keys_ feature to quickly move around a text document.
 
