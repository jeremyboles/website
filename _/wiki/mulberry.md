# Mulberry
## A Raspberry Pi 4 Model B.

---

The single-board computer that sits on my desk in the studio and acts as a playground for ARM computing and as a node for file syncing via [[Syncthing]] to the rest of the dinguses. The board itself lives in a Miuzei aluminum case and is sometimes hooked up to a generic 7-inch HDMI display.


### Tíamda Specs

- **OS**: [[Alpine Linux]] 3.21
- **Display**: 10-inch @ 102 DPI (1024×600)
- **Disk**: SanDisk 128 GB microSDXC
- **CPU**: Broadcom BCM2711 Cortex-A72 @ 1.8GHz (×4)
- **RAM**: 2 GB LPDDR4-3200 
- **Model number**: Raspberry Pi 4 Model B Rev 1.2
 
