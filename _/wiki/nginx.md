# Nginx
## An efficient, reliable, & robust web server.
---

Nginx serves roughly a third of all websites[^1]. While it is often used as a _reverse proxy_ for larger, dynamic web applications, it also accels at being a basic web server, serving static websites. 

### Serve HTML files without the suffix

I like the URLs on my website be short, and without the `.html` suffix. To have Nginx serve up the HTML files without their suffix in the URL, I add the following to the `server` block of my `nginx.conf` file:

```nginx.conf
location / {
	if ($request)_uri ~ ~/(.*)(\.html|/)$) {
		return 301 /$1;
	}
	try_files $url $uri.html $url/ = 404;
}
```

### Serve precompressed static assets

Since I serve my website from an low-end VPS server, I like to give the CPU a break and precompress the files before I upload them, instead of having Nginx do it on the fly. Nginx supports gzip files out of the box and most browsers support gzipped content. To have Nginx serve content encoded with some of the newer compression algorimths that some browsers support---[Brotli](https://en.wikipedia.org/wiki/Brotli) and [zstd](https://en.wikipedia.org/wiki/Zstd)---we'll need to install some Nginx modules. To install the modules on [[Alpine Linux]], run:

```sh
doas apk add nginx-mod-http-brotli nginx-mod-http-zstd
```

To have Nginx load the modules, add the following `load_module` statements to the top of your `nginx.conf` file:

```nginx.conf
load_module /usr/lib/nginx/modules/ngx_http_brotli_static_module.so;
load_module /usr/lib/nginx/modules/ngx_http_zstd_static_module.so;
```

Now, to add support for precompressed Brotli, gzip, and zstd compression, add the following to the `server` block of your `nginx.conf` file:

```nginx.conf
brotli_static on;
gzip_static on;
zstd_static on
```

Reload nginx by running `doas rc-service nginx relload`. Now, when you look at the response headers being sent from your website, you should see something like `Content-Encoding: zstd` being sent.

[^1]: https://en.wikipedia.org/wiki/Nginx#Popularity
