# Color Scheme

---

I maintain a personal color scheme that I use on my projects. It is a derivative of the [Radix color scheme](https://www.radix-ui.com/colors), using the same color names and shade steps, but with the hues tweaked closer to my own tastes, that is to say _warmer_. Each color and its shade scale has a both a light and dark version.


{{ define "head" }}
	<meta name="go-import" content="jeremyboles.com/colors git https://git.sr.ht/~jeremyboles/colors">
{{ end }}

