# Owl
## A fantasy word-processor appliance.

---

Owl is an experimental project that aims to turn old laptops into [[word processor]] appliances.

### Motivation

The world is littered old computers that are tucked away in drawers, closets, or water, tossed into the trash. Computers have been good at word processing for decades now, but most word processing software is optimized for business use and includes many features that most writers do not need.

At the same time a desire exists for software that when doing creatve work, like writing, removes distractions. Working from modern computers and operating systems often means having to ignore notifications about emails, social media, or todo-list reminders that can distract a writer from getting into a flow state. Constantly resisting the urge to open up a web browser erodes at one's stamina that is need to focus on an ambitious writing project.

### Links

- [Source code](https://git.sr.ht/~jeremyboles/wattle)

