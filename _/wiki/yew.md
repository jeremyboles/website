# Yew
## A Lenovo ThinkPad X280

---

I bought this [[ThinkPad x280]] laptop off eBay in January of 2025. Once I started planning a comming trek to England, I decided that it'd be helpful to have a lighter weight laptop than [[Thinkpad X220i]] and that being able to charge via a USB-C port would be helpful for cutting down cord clutter while traveling. I added a massive disk before installing Alpine Linux so that there was room for any photos and videos that I make take on the trip. 

### Yew Specs

- **OS**: [[alpine|Alpine Linux]] 3.21
- **Window manager:** [[awesomewm|Awesome]]
- **Display**: 12.5-inch @ 125 DPI (1366×768)
- **Disk**: 4 TB NVMe SSD
- **CPU**: Intel Core i5-8350U @ 1.7 GHz (×8)
- **GPU**: Intel UHD Graphics 620
- **RAM**: 16 GB DDR4-2400
- **Model number**: TP00093A

### Known Issues

* the <kbd>"</kbd> key doesn't always respond on first press
* the USB-A port on the left-side does not seem to work
* the USB-A port on the right-side is bent and my [Yubikey](Yubikey) sticks out a little bit instead of being fully seated
