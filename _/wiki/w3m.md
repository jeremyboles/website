# w3m
## A text-focused [[browsers|browser]] for rendering web pages in a terminal.

---

**w3m** runs in a text-only environment and, as such, it does not support CSS or Javascript. However, it does have support for some other basic features of the web, namely: bookmarks, cookies, forms, and even HTTPS.

[w3m][w3m] is open source software that was originally written by Akinori Ito and with new features and maintenance done by [Tatsuya Kinoshita][tats] for the [[debian|Debian project]] and a [modest list of contributors][contrib]. The Debian project seems to be the main driver of the continued maintenance of w3m.

## Using w3m

w3m is great for focused, distraction-free web surfing. Small-web and independent websites tend to have much better support for w3m and Javascript-free browsing. It is also great for accessing the web on lower-powered machine, even ones that can handle rendering a desktop environment well but simply cannot cope with the resource requirements required to run the modern, mainstream browsers.

w3m is useful for accessing the web over SSH when doing maintenance on a remote server. It is handy to be able to quickly look up a command or configuration option when logged into a remove server.

w3m is often used for displaying HTML emails in [[mutt]], a command-line email client. Many emails are sent without a plain-text alternative these days. Most non-HTML is sent by marketers and can be simply be ignored, but I've found that email coming from Apple's email client often does not have a text version included anymore. w3m, however, does a great job at rendering the emails sent from those clients.

### Daily-Driving w3m

With features like bookmarks, HTTPS, and cookies, w3m is very capable of being a primary browser. However, often times (er, most of the time) web pages have never been tested in w3m so the layouts of websites can be hard to read.

[DuckDuckGo](https://duckduckgo.com/) works well with w3m for searching the web. Google also works mostly fine too, if you are okay with it replacing all of the search URLs with Google tracking links.

### Image Support

Although w3m is a console browser, it can display display images using [sixels](https://en.wikipedia.org/wiki/Sixel) in terminals that support it.

## Developing with w3m in Mind

w3m has good support for HTML tables, so channeling your 90s-era web design skill and relaxing any dogma you might be harboring about semantic HTML _just a bit_ will help in making usable interfaces that work with w3m. Keep in mind that when using any structural tag for presentational purposes, it is important to add `role="none"` or `role="presentation"` to the element so browsers knows not to assign any semantic meaning to the tag.

For certain interfaces, I find that wrapping the entire contents of the website in a `<center>` tag and then within a `<table>` tag makes the website easier to read. I like to keep the [lengths of long lines of text to a reasonable number][line-length] and I find that setting a width of `624` on the wrapping`<table>` tag keeps the lines to just about 72 characters long, which is comfortable for my eyes. Remember to use a `<tbody>` tag in your tables because modern graphic browsers will inject a node for one into the DOM anyways, even if tag itself is absent in your code. To have a modern browser completely ignore these exploited presentational tags, add `display: contents` to remove them from content flow.

---

### Further Reading

- [ArchLinux's w3m Wiki Entry](https://wiki.archlinux.org/title/W3m)
- [Vitaly Parnas's w3m General Strategies](https://vitalyparnas.com/guides/w3m/)

[contrib]: https://github.com/tats/w3m/graphs/contributors
[tats]: https://github.com/tats/w3m
[line-length]: https://practicaltypography.com/line-length.html
[w3m]: https://w3m.sourceforge.net/

