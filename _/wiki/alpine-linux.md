# Alpine Linux
## A lightweight Linux distribution

---

### Installing Alpine Linux

Installing Alpine Linux is straight forward when using the `setup-alpine` script that comes on the standard ISOs. 

On older computers, its often a good idea to have some kind of USB network adapter handy incase the internal WiFi or the onboard Ethernet port aren't recognized during installation. _Note to self: try the extended ISO to see the built-in networking cards are recognized._

Plug in a USB key that has the Alpine Linux ISO written to it then turn on the computer and boot into the BIOS.

After the computer reboots, it should load the Alpine Linux installer. After some text moves across the screen there will be a prompt that reads `localhost login:` with a blinking cursor. Type in `root` and then press <kdb>Enter</kdb> to log into the installer. Type `setup-alpine` to start the installation process. Follow the prompts, paying close attention to the *Hostname*, *Timezone*, and *User* prompts.

When you get to the *Disk & Install* prompt, type in `sda` to choose the internal hard drive, then type `crypt` to enable disk encryption, and `sys` to use the disk as a system disk.s. When you get to the *Disk & Install* prompt, type in `sda` to choose the internal hard drive, then type `crypt` to enable disk encryption, and `sys` to use the disk as a system disk. Type in a good, strong password three times---twice to the set it and once to unlock the disk---and then let the installation process proceed. After it has completed type in `reboot` and then remove the USB to boot into the new Alpine Linux install.

