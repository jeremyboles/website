# Dinguses
## Various electronic devices and servers that I maintain for work, play, & research.

---

I have an excessive number of gadgets and dinguses in my studio. This is partly because I enjoy seeing how much work I can do on the older devices that used to be sought after and are now thrown in drawers and even into the trash. The other part is that I want to have extra computers in reserve for when the one I am currently using wears out. Another part is that I, sometimes, cannot help myself and I pick up a computer being sold for $10 or so that used to be a multiple hundreds---_or thosands!_---of dollars back when I started intially playing with computers.

### Laptops

- [[Yew]] - *a Lenovo ThinkPad X280*
- [[Holly]] - *a Samsung N130*

### Servers

- [[Oak]] - *a cheap-ass virtual private server*
- [[Mulberry]] - *a Raspberry Pi 4 Model B*

