# Colophon

---

This website is a _living document_ and, as such, needs both flexible software and flexible design that stay out of the way and allow thoughts and ideas to grow and for their tendrils to entangle themselves organically.

### Software

Simplicity is a goal for the development of this website. The output of this website consists of a collection of HTML files, optimized images, and a CSS file. I am not opposed to the inclusion of a small sprinkling Javascript to add extra functionality to this website, but as it stands right now there is none used on this website.

The website is compiled together by a simple program called **[[wattle]]** that is written in Go. The program collects and compiles various [[Markdown]] and templates files into an output directory. The included `Makefile` includes a rule for uploading that output directory to a server running [[Nginx]], thus putting them on the world-wide-web.

### Design & Typography

The **layout** of the website keeps a targeted `22px` vertical rhythm using `em` units allow folks visiting to scale the size to fit their own needs while still maintaining a consistent scale. The typographic scale is set on a `8:9` major second scale, again using `em` units in CSS. The use of sidenotes and marginalia is inspired by the books of [Edward Tufte](https://www.edwardtufte.com/).

The main **typography** for the website is displayed using the font [Newreader](https://github.com/productiontype/Newsreader)---if it is available---followed by the _[Old Style](https://github.com/system-fonts/modern-font-stacks?tab=readme-ov-file#old-style)_ stack from [Modern Font Stacks](https://modernfontstacks.com/). The _[Neo-Grotesque](https://github.com/system-fonts/modern-font-stacks?tab=readme-ov-file#neo-grotesque)_ and _[Monospace Code](https://github.com/system-fonts/modern-font-stacks?tab=readme-ov-file#neo-grotesque)_ stacks from Modern Font Stacks are also used in certain places for contrast.

### Links

- [Source code](https://codeberg.org/jeremyboles/website)

{{ if ne .Builder.Context.Mode "archive" }}

---

A compressed archive of my website is continuously created and can be downloaded for offline use: ![[download:archive.tar.gz]].

{{ end }}

