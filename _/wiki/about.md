# About Jeremy
## "...he should have been sent back to the factory and fixed."

---

Hello there. My name is Jeremy Boles and I reside in a small city called _Springfield_ that is in the Southwest of Missouri, United States. Most of the time I would rather be somewhere else though. I generally work out of a studio made from of an old camper which sits in the backyard of the small, 1920s-era bungalow that I live in with my family.

---

Send email to [me@jeremyboles.com](mailto:me@jeremyboles.com). Feel free to use my [public GPG key](https://jeremyboles.com/public.gpg) when doing so.
