# Programs & Applications
## Personal projects involving computers.

---

I almost always have a few _in-progress_ projects and ideas [rattling](rattling) around in my head that involve [[computers]].

### Planning & Research Phase

- **[[Owl]]** -- a [[word processor]] appliance that uses salvaged, old laptops and [[netbooks]] to create a distraction-free tool for writing prose

### Somday/Maybe

- **Booke Club** -- turning the world into an invisible, free library
- **Outside** -- a passive game to play while moving about
- **Narrated World** -- hear stories about wherever you are in the world
- **robot.ink** -- a thermal printer that can have messages sent to it from the internet
