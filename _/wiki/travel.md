# Travel
## "I've got pictures to prove I was there, but you don't care."

---

Travel, for me, is a shortcut along the path to a fruitful creative state. It is costly but, I think, worth budgeting for. Through travel I have developed a sense of empathy, learned new ways of being resourceful, and even experienced glimpses of contentment.

### Europe

![PHOTO: The Broel Towers spanning the river Lys in Kortrijk, Belgium](image:belgium/travel)
[[Belgium]] *2022*

![PHOTO: A path through the Muesum Gardens in York, England](image:england/travel)
[[England]] *2014, 2023, 2024*

![PHOTO: The Mansard roofs of Paris, France](image:france/travel)
[[France]] *2014, 2022*

![PHOTO: A gravestone at the Rock of Cashel, Ireland](image:ireland/travel)
[[Ireland]] *2013, 2015, 2017, 2022*

![PHOTO: The sunsetting on the harbor of Vernazza, Italy](image:italy/travel)
[[Italy]] *2013, 2015, 2017, 2018, 2023*

![PHOTO: A man working from a laptop while on a train at Amsterdam Centraal](image:netherlands/travel)
[[The Netherlands]] *2014, 2022*

![PHOTO: The Forth Bridge towering over Queensferry, Scotland](image:scotland/travel)
[[Scotland]] *2019*

### North America

![PHOTO: A desolate highway on the way to the Grand Canyon](image:arizona/travel)
[[Arizona]] *2012*

![PHOTO: A blue sky over Bear Lake in Rocky Mounain National Park](image:colorado/travel)
[[Colorado]] *2007-2009, 2012, 2014, 2022*

![PHOTO: An old train bridge in rural Vermont](image:vermont/travel)
[[Vermont]] *2013*
