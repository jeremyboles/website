# Go
## A productive, portable, general purpose programming language. 

---

Go is an interesting language that sits somewhere in between a high-level scripting language and a lower-level systems-type language. Compiled Go programs include a heavy-weight runtime that includes features like reflection, garbage collection, and concurrency.

I find that Go is a reasonable first choice when creating a personal tool. I can be very productive quickly and, in the end, I will have a binary that can run in a lot of different places without too much fuss. That being said, Go binary sizes are [generally acknowledged](https://github.com/golang/go/issues/6853) to be bloated. Go prioritises performance at the cost of memory, embedding considerable amounts of runtime information in produced binaries. Passing flags such as `-ldflags="-s -w"` to `go build` can help reduce the size of the binary a bit, if desired.

### Installation

To install Go on [[Alpine Linux]], simply run:

```sh
does apk add go
```
