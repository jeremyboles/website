# Netsurf
## Netsurf is an independent, lightweight web browser.

---

Netsurf is an actively developed, independent browser that has good support for CSS 2.1 features---before flexbox and grid---along with support for some helpful newer CSS features, such as `@media` queries.

## Notes on Supporting Netsurf

Netsurf does not support `display: flex` but that does not need to be a problem. Float-based layouts---_and table-based layouts, if need be_---still work in typical browser but also work in Netsurf. Buy a web design from the early 2000s and see what kind of layouts are possible.

Netsurf ignores CSS within a `@supports` block. If a layout cannot fully be accomplished with floats alone you can start with a base layout for Netsurf and then _progressively enhance_ the design for browsers that support new CSS features.

Use `@media` queries with `px` values to adjust layouts for smaller screens or window sizes. Often this means removing a float when a screen is too small so items stack instead of being display side-by-side.

Netsurf only supports generic font family names and will not display custom fonts, even if they are installed on the system that is running Netsurf. Learning to use __design fundamentals__ like contrast and spacing is not only a great design exercise, it a great place to start a design to make sure it is sound and not relying on current fashion trends in design and typography. Netsurf supports the following generic font families:
- `sans-serif`
- `serif`
- `monospace`
- `cursive`
- `fantasy`
- `default`

