# What I'm Doing Now

---

At the moment, I am focused on:

- **Creating this website:** The [[wattle|tooling]] that I have created to help weave my website together _feels_ finished. Now all that is left is the writing, which is really the whole point. Am spending many of my mornings collecting my thoughts, reflecting on what I know, and then getting things down into words.
- **Preparing for Chester:**  I am going to be in Chester for a week this month. My plan is to work from [[yew|an old computer]] of mine, so I've been making sure that its set up properly so I can work offline for a spell.
- **Brainstorming:** I've finally started to collection my thought in the form of writing code for a project that, in reality, has been on my mind for the better part of seven years. The project involves creating a [[owl|word processor appliance]] out of old computers and this project, in particual, has helped morph me into who I am today.
