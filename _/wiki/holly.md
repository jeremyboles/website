# Holly
## A salvaged Samsung NP-N130 netbook.

---

The [[netbooks|netbook]] was acquired from eBay for research on low-spec and [[salvage computing]]. The comfort of the computer's keyboard and contrast of the screen have been a nice surprise which has led to the computer to be used for more than research and for general computing as well.

### Holly specs

- **OS**: [[Alpine Linux]] 3.21
- **Display**: 10-inch @ 102 DPI (1024×600)
- **Disk**: 160 GB HDD @ 5400 RPM
- **CPU**: Intel Atom N270 @ 1.6 GHz (×2)
- **GPU**: Intel Mobile 945GSE Express
- **RAM**: 1 GB DDR2-533
- **Model number**: NP-N130

### Notes on installing Alpine Linux

_**The installation took 5 minutes to complete on Holly**. A previous bare-bones Debian Linux installation took over 50 minutes on the same computer._

Some kind of USB network adapter will be needed as both the internal WiFi and the onboard Ethernet port aren't recognized during installation. _Note to self: try the extended ISO to see the built-in networking cards are recognized._
