# Wattle
## A bespoke website builder. 

---

Wattle is the name of the computer program that builds my website. It is written in the [[go|Go programming language]]---although, it does make systems calls to a handful external programs---and is designed to run on the [[Alpine Linux]]. The name of Wattle's binary executable is `tim` and it is built to run on older 32-bit systems like [[netbooks]], 64-bit systems like most computers one would buy today, as well as single-board ARM computers like a Raspberry Pi or any of its clones that can run Alpine Linux.


### System Architecture

Like many [static website generators][ssg], Wattle takes a collection of input files and transform them into a website. However, Wattle was designed to not only process text files, such and Markdown and HTML, but to process _any_ sort of input file, including media files such as audio, images, and video.

To accomplish this in a maintainable way, Wattle, compiles the website in three phases: ***gather***, ***process***, and ***transform***.

1. During the *gather* phase, a group of input directories are walked through, file-by-file, looking for ones to add to the website. Any file that will be part of the website is called an ***artifact***.
2. Next, in the *process* phase, each artifact is inspected and what ever is needed to provide information about its final state is done. An artifact can have more than one output file and this is determined at this stage. For instance, a photo can have a smaller thumbnail in addition to a full-sized version.
3. Finally, the *transform* phase turns the artifact into its final forms, including any optimization that can be done to the final file to prepare it for the web. This stage utilizes Go's `io.Writer` interface to provide a standard way to support multiple output types.


### Program Usage

The `wattle` executable has three main commands:

* `wattle archive` - runs all three phases and then creates a `.tar.gz` file that contains the entire website.
* `wattle build` - runs all three phases and then compiles the website into an output directory that could have a web serve pointed at it.
* `wattle serve` - runs the *gather* and *process* phases and then starts an HTTP server that performs the *transform* phase on the fly as an artifact is requested.

In order for the certain builders to run, a few binaries need to be available:

```sh
doas apk add jpegoptim optipng pngquant zopfli
```

### Links

- [Source code](https://git.sr.ht/~jeremyboles/wattle)

[ssg]: https://en.wikipedia.org/wiki/Static_site_generator
